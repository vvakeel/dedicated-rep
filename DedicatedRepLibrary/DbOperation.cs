﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using MySql.Data.MySqlClient;


namespace DedicatedRepLibrary
{
 public class DbOperation
    {
        
     
     scrapdbEntities db = new scrapdbEntities(); //this is for entity framework object for calling of collections.
     
     public List<customer> SelectCustomer() 
     {
         List<customer> cs = (from c in db.customers
                              where c.Active == true
                               select c).ToList();                    
         return cs;
     }

     public customer SelectSingleCustomer(int id) // For Edit Customer 
     {
         customer cs = (from c in db.customers
               where c.CustomerId == id
               select c).SingleOrDefault<customer>();
         return cs;
     }

     public void UpdateCustomer(customer cs)
     {
         db.Entry(cs).State = EntityState.Modified;
         db.SaveChanges();
     }
     public void CreateCustomer(customer cus)
     {
         cus.Active = true;
         db.customers.Add(cus);
         db.SaveChanges();
     }

     public void DeleteCustomer(customer cus)
     {
         cus.Active = false;
         db.Entry(cus).State = EntityState.Modified;
         db.SaveChanges();
     }


    }
}
