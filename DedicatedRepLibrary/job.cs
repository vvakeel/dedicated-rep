//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DedicatedRepLibrary
{
    using System;
    using System.Collections.Generic;
    
    public partial class job
    {
        public int JobId { get; set; }
        public string JobTitle { get; set; }
        public string JobCode { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string JobType { get; set; }
        public string JobDetail { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public System.DateTime UpdateDate { get; set; }
        public string AccountDID { get; set; }
        public string JobNote { get; set; }
        public string ApplyUrl { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string CompanyName { get; set; }
        public string Category { get; set; }
        public string JobRequirements { get; set; }
        public int CustomerId { get; set; }
    
        public virtual customer customer { get; set; }
    }
}
