﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DedicatedRepLibrary;
using System.Net;

namespace DedicatedRep.Controllers
{
    public class HomeController : Controller
    {
        DbOperation db = new DbOperation();
        //
        // GET: /Home/

        public ActionResult Index()
        {
            List<customer> cs= db.SelectCustomer();
            return View(cs);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
           customer cs = db.SelectSingleCustomer(id);
            return View(cs);
        }

        [HttpPost ]
        public ActionResult Edit(customer Cus)
        {
            if (ModelState.IsValid) //used to validate the fields data and datatype with models(respectively)
            {
                db.UpdateCustomer(Cus); 
                return RedirectToAction("Index");//to redirect the page after operation.
            }
            return View(Cus);
        }

        [HttpGet]
        public ActionResult Details(int id) 
        {
            customer cs = db.SelectSingleCustomer(id);
            return View(cs);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
 
        }

        [HttpPost]
        [ActionName("Create")] //the actionResult of the controller identifies by the ActionName mentioned here,                               //if we have different actionName in the Method like:-Create_Insert().
        public ActionResult Create_Insert(customer cus )
        {
            if (ModelState.IsValid) //used to validate the fields data and datatype with models(respectively)
            {
                db.CreateCustomer(cus);//to create the Row in the Customer Table.
                return RedirectToAction("Index");//to redirect the page after operation.
            }
            return View(cus);
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            customer cs = db.SelectSingleCustomer(id);
            if (cs == null)
            {
                return HttpNotFound();
            }
            return View(cs);
        }

        [HttpPost] 
        [ActionName("Delete")] 
        public ActionResult Delete_Customer(int id)
        {
            if (ModelState.IsValid) //used to validate the fields data and datatype with models(respectively)
            {
                customer cs = db.SelectSingleCustomer(id);
                db.DeleteCustomer(cs);//to Delete the Row in the Customer Table not physically but by making active field "False i.e. 0".
                return RedirectToAction("Index");//to redirect the page after operation.
            }
            return RedirectToAction("Index"); // to redirect the page after Operation.
        }
        
      }
}
